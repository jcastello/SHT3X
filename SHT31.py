# Basé sur le code de : https://www.controleverything.com/content/Humidity?sku=SHT31_I2CS#tabs-0-product_tabset-2
import smbus2 as smbus
import time



class SHT31():
    # SHT31 Command Set
    SHT31_MEAS_REP_STRETCH_EN			= 0x2C # Clock stretching enabled
    SHT31_MEAS_HIGH_REP_STRETCH_EN		= 0x06 # High repeatability measurement with clock stretching enabled
    SHT31_MEAS_MED_REP_STRETCH_EN		= 0x0D # Medium repeatability measurement with clock stretching enabled
    SHT31_MEAS_LOW_REP_STRETCH_EN		= 0x10 # Low repeatability measurement with clock stretching enabled
    SHT31_MEAS_REP_STRETCH_DS			= 0x24 # Clock stretching disabled
    SHT31_MEAS_HIGH_REP_STRETCH_DS		= 0x00 # High repeatability measurement with clock stretching disabled
    SHT31_MEAS_MED_REP_STRETCH_DS		= 0x0B # Medium repeatability measurement with clock stretching disabled
    SHT31_MEAS_LOW_REP_STRETCH_DS		= 0x16 # Low repeatability measurement with clock stretching disabled
    SHT31_CMD_READSTATUS				= 0xF32D # Command to read out the status register
    SHT31_CMD_CLEARSTATUS				= 0x3041 # Command to clear the status register
    SHT31_CMD_SOFTRESET					= 0x30A2 # Soft reset command
    SHT31_CMD_HEATERENABLE				= 0x306D # Heater enable command
    SHT31_CMD_HEATERDISABLE				= 0x3066 # Heater disable command


    def __init__(self, i2c_bus=1, i2c_adress=0x44):
        self.i2c_bus = i2c_bus
        self.i2c_adress = i2c_adress

    def write_command(self):
        """Select the temperature & humidity command from the given provided values"""
        
        COMMAND = [SHT31.SHT31_MEAS_HIGH_REP_STRETCH_EN]
        with smbus.SMBus(self.i2c_bus) as bus:
            bus.write_i2c_block_data(self.i2c_adress,SHT31. SHT31_MEAS_REP_STRETCH_EN, COMMAND)

    def read_data(self):
        """Read data back from device address, 6 bytes
        temp MSB, temp LSB, temp CRC, humidity MSB, humidity LSB, humidity CRC"""
        self.write_command()
        time.sleep(0.001)
        with smbus.SMBus(self.i2c_bus) as bus:
            data = bus.read_i2c_block_data(self.i2c_adress, 0, 6)
        
        # Convert the data
        temp = data[0] * 256 + data[1]
        temp_c = -45 + (175 * temp / 65535.0)
        temp_f = -49 + (315 * temp / 65535.0)
        humidity = 100 * (data[3] * 256 + data[4]) / 65535.0
        
        return {'temperature_c' : temp_c, 'temperature_f' : temp_f, 'humidity' : humidity}


if __name__ == "__main__":

    sht31 = SHT31(i2c_bus=1, i2c_adress=0x44)

    while True:
        result = sht31.read_data()
        print("Relative Humidity : %.2f %%"%(result['humidity']))
        print("Temperature in Celsius : %.2f C"%(result['temperature_c']))
        print("Temperature in Fahrenheit : %.2f F"%(result['temperature_f']))
        print(" ************************************* ")
        time.sleep(5)

